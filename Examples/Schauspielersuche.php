<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="./syles.css">
<html>
<h1>Navigation FilmDB</h1>
<ul class="nav nav-tabs">
    <li  class="nav-item">
        <a class="nav-link" href="./Filmsuche.html">Suche nach Produktionsfirma</a>
    </li>
    <li  class="nav-item">
        <a class="nav-link active" href="./Schauspielersuche.html">Suche nach Schauspieler</a>
    </li>
</ul>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: Lukas Falkensteiner
 * Date: 2/14/2018
 * Time: 2:35 PM
 */
$host="127.0.0.1";
$port=3306;
$socket="";
$user="root";
$password="";
$dbname="moviedb";

$con = new mysqli($host, $user, $password, $dbname, $port, $socket)
or die ('Could not connect to the database server' . mysqli_connect_error());

if (isset($_GET["schauspieler"])) {
    $query_actor = $_GET["schauspieler"];

    $sql = "Select distinct ActorName from moviedb.actor
                where ActorName like \"%" . $query_actor ."%\";";
    $result = $con->query($sql);
    if ($result->num_rows > 0) {
        // output data of each row
        echo "Gefundene Schauspieler: ";
        while ($row = $result->fetch_assoc()) {
            echo $row["ActorName"] . ", ";

        }
    } else {
        echo "FilmStudio " . $query_actor . " nicht gefunden!";
    }


    $sql = "Select a.ActorName as Schauspieler, f.MovieName as Titel, f.ReleaseDate as Erscheinungsdatum, p.StudioName as Bezeichnung from moviedb.movie f, moviedb.moviestudio p, moviedb.actor a, moviedb.starring s
        where a.ActorName LIKE \"%" . $query_actor ."%\"
        and s.idActor = a.idActor
        and s.idMovie = f.idMovie
        and f.idMovieStudio = p.idMovieStudio
        order by a.ActorName;";


    $result = $con->query($sql);

    echo "<br>";
    echo "Gefundene Filmtitel: " . $result->num_rows . "<br>";

    echo "<table class='table table-bordered'>";
    if ($result->num_rows > 0) {
        // output data of each row
        echo "<thead><tr>";
        echo "<th scope ='col'>Schauspieler</th>";
        echo "<th scope ='col'>Titel</th>";
        echo "<th scope ='col'>Erscheinungsdatum</th>";
        echo "<th scope ='col'>Bezeichnung</th>";
        echo "</tr></thead><tbody>";
        while ($row = $result->fetch_assoc()) {
            echo "<tr>
                        <td class=''>". $row["Schauspieler"] ."</td>
                        <td class=''>". $row["Titel"] ."</td>
                        <td>". $row["Erscheinungsdatum"] ."</td>
                        <td>". $row["Bezeichnung"] ."</td>
                        </tr>";
        }
        echo "</tbody></table>";
    } else {
        echo "0 results";
    }

}

$con->close();
