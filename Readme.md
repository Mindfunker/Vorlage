# Informationstechnologie- Informatik

This is some examples I put together

## Getting Started


### Installing

Programs needed 

 *  [xampp](https://www.apachefriends.org/index.html) - php dev environment 
 *  [phpstorm](https://www.jetbrains.com/phpstorm/?fromMenu) - IDE
 *  [mysql workbench](https://www.mysql.com/products/workbench/) - Mysql
 *  [google chrome](https://www.google.ca/chrome/browser/desktop/index.html) - Browser 
 
 Optional 
 * [notepadd++](https://notepad-plus-plus.org/download/v7.5.4.html) - Text editor
 * [git] (https://git-scm.com/downloads) - source code manager
  

Start Xampp

```
Click start
```

Configure Intellij php interpreter. 

```
C:\xampp\php\php.exe
```

Connect workbench

```
user: root 
password: 
```

### Templates

 * [Doc](/Documentation/README.md) - Documentation

## Helpful Links

* [w3 php](https://www.w3schools.com/php/default.asp) - PHP 
* [w3 sql](https://www.w3schools.com/sql/default.asp) - SQL 
* [ws bootstrap](https://www.w3schools.com/bootstrap/default.asp) - Bootstrap framwork